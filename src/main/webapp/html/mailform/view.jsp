<%@ include file="init.jsp"%>
<script src="https://www.google.com/recaptcha/api.js" async defer></script>


<portlet:actionURL var="validateURL" name="submitSuggestion" />


<aui:form action="<%=validateURL%>" method="post" name="fm">
	<h3>Your input is valuable to us, please send us your suggestions.</h3>
	<aui:spacer></aui:spacer>
	<aui:input name="name" label="Please enter your name" value=""
		required="true">
		
	</aui:input>
	<aui:input name="email" label="Email" value="" required="true">
		<aui:validator name="email" />
	</aui:input>
	<aui:input type="textarea" name="message" label="Message" value=""
		required="true" rows="6" style="width:300px;" />

	<div class="g-recaptcha" data-sitekey="6Ld0hfYZAAAAAKlIHde1v3QE4NyY4AniN6j-U9SP"></div>

	<aui:button-row>
		<aui:button value="Submit suggestion" type="submit"
				cssClass="btn-large btn-fill" />
	</aui:button-row>

</aui:form>
<script type="text/javascript">
    AUI().use(
        'aui-tooltip', 'aui-base', 'selector-css3',
        function (A) {
            var form = A.one('#<portlet:namespace />fm');
            if (form) {
                form.on(
                    'submit',
                    function(event) {
                    	  if (grecaptcha.getResponse() === '') {              
                            event.halt();
                            event.stopImmediatePropagation();
                            alert('Please prove you are not a robot');
                    	  }
                    }
                );
            }
        });
</script>
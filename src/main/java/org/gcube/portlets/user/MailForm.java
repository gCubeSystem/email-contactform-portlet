package org.gcube.portlets.user;

import java.io.IOException;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.portlet.ProcessAction;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.servlet.http.HttpServletRequest;

import org.gcube.common.portal.mailing.EmailNotification;

import com.liferay.portal.kernel.captcha.CaptchaException;
import com.liferay.portal.kernel.captcha.CaptchaUtil;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.util.PortalUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
/**
 * Portlet implementation class CaptchaDemo
 */
public class MailForm extends MVCPortlet {

	public void submitSuggestion(ActionRequest actionRequest,
			ActionResponse actionResponse) throws IOException, PortletException {

		String name = ParamUtil.getString(actionRequest, "name");
		String email = ParamUtil.getString(actionRequest, "email");
		String message = ParamUtil.getString(actionRequest, "message");

		System.out.println("name   :" + name);
		System.out.println("Email  :" + email);
		System.out.println("message  :" + message);

		HttpServletRequest request = PortalUtil.getHttpServletRequest(actionRequest);
		String hostname = PortalUtil.getHost(actionRequest);
		System.out.println("hostname to split:" + hostname);
		String emailAddress = "info@d4science.org";
		if (hostname.equalsIgnoreCase("gcube-system.org")) {
			emailAddress = "info@gcube-system.org";
		} else {
			String[] splits =  hostname.split("www.");
			if (splits.length > 1)
				emailAddress = "info@"+ hostname.split("www.")[1];
		}
		String bodyTextHTML = "<p>From: " + name + "</p>";
		bodyTextHTML += "<p>Email: " + email + "</p>";
		bodyTextHTML += "<p>Message:</p><p> " + message + "</p>";

		String validEmail = "info@d4science.org";
		//fallback
		if (isValidEmailAddress(emailAddress)) {
			validEmail = emailAddress;
		}
		EmailNotification emailNotification = new EmailNotification(validEmail, "Feedback from Contact us Web Form", bodyTextHTML, request);
		emailNotification.sendEmail();
	}

	public static boolean isValidEmailAddress(String email) {
		boolean result = true;
		try {
			InternetAddress emailAddr = new InternetAddress(email);
			emailAddr.validate();
		} catch (AddressException ex) {
			result = false;
		}
		return result;
	}


}
